# Review Questions

## Question 1: Which point(s) were new to you?

All the points mentioned in the section on "Doing things with 100% involvement" were new to me. Particularly, the emphasis on preserving attention as a skill, the recommendation to read up on deep work, and the suggestion to use time-tracking apps for improving productivity caught my attention.

## Question 2: Which area do you think you need to improve on? What are your ideas to make progress in that area?

## Areas for Improvement:

- **Communication with Higher-Positioned Employees:** Strive to improve communication with higher-level colleagues by maintaining a balance between professional and casual communication styles based on the context.

## Ideas for Progress:

- **Balance:** Maintain a balance between professional and casual communication styles.
- **Active Listening:** Engage in active listening to better understand others' perspectives.
- **Clear and Concise Communication:** Ensure clarity and avoid jargon in your communication.
- **Preparation:** Prepare for meetings by familiarizing yourself with the agenda and taking notes during discussions.
- **Follow-Up:** Follow up on meetings with relevant information and action items.

