# Grit and Growth Mindset
### 1. Paraphrase (summarize) the video in a few (1 or 2) lines. Use your own words.
Grit means not giving up, even when things are really hard. It's about staying strong and pushing through difficulties to reach your goals.
### 2. Paraphrase (summarize) the video in a few (1 or 2) lines in your own words.
A growth mindset means thinking you can improve if you keep trying and don't give up. It's about believing in yourself and learning from mistakes.
### 3. What is the Internal Locus of Control? What is the key point in the video?
Internal locus of control means feeling like you can control your life. In the video, they say it's important to believe you can shape your own future.
### 4. What are the key points mentioned by speaker to build growth mindset (explanation not needed).
+ Learn from mistakes.
+ Be inspired by other's success.
+ Stay strong when things don't go as planned.
+ Believe in the power of improvement.
### 5. What are your ideas to take action and build Growth Mindset?
+ I will understand each concept properly.
+ I will stay relaxed and focused no matter what happens.
+ I am 100 percent responsible for my learning.
+ Change "I can't" to "I'll try."