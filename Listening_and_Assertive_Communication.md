## What are the steps/strategies to do Active Listening?
+ Stay focused.
+ Don’t interrupt.
+ Listen without judging, or jumping to conclusions.
+ Show that you’re listening.
+ Stay focused.
+ Face the speaker and have eye contact.
+ Ask questions.
+ Give full attention.
## According to Fisher's model, what are the key points of Reflective Listening?
+ Listen to the speaker's message.
+ Analyze the meaning of the speaker's message. 
+ Reflect the message back to the speaker.
+ Confirm that you properly understood the message.
## What are the obstacles in your listening process?
+ Lack of Interest.
+ Emotional Barriers.
+ Distractions.
+ Cultural Differences.
+ Poor Listening Habits.
## What can you do to improve your listening?
+ Continuous learning.
+ Practice empathy.
+ Ask clarifying questions.
+ Be open-minded.
+ Seek feedback.
## When do you switch to Passive communication style in your day to day life?
+ When I talking with my family or friends.
+ When I avoid conflicts.
## When do you switch into Aggressive communication styles in your day to day life?
+ When I feeling disrespected or ignored.
+ In moments of frustration or anger.
## When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?
When I'm unable to address issues directly, I might resort to passive-aggressive communication styles like giving the silent treatment to express my dissatisfaction indirectly.
## How can you make your communication assertive? You can watch and analyse the videos, then think what would be a few steps you can apply in your own life?
To improve my communication assertive, I will do following activities:
+ Practice Assertive Body Language.
+ Practice Active Listening.
+ Give and Receive Feedback.
+ Express Needs and Preferences.
+ Stand Up for Myself.
+ Can't wait to ask something or give suggestions. 


