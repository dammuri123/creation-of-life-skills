# Energy Management
### 1. What are the activities you do that make you relax - Calm quadrant?
* Listening to music.
* Spending time with loved ones.
* Playing games like Cricket and Carroms.
### 2. When do you find getting into the Stress quadrant?
* Dealing with unexpected problems or conflicts.
* Taking on too many responsibilities at once.
* Approaching deadlines.
### 3. How do you understand if you are in the Excitement quadrant?
* When I feeling positive and ready to face challenges.
* when I eagerly looking forward to what's next.
* When I'm in accelerated mental state.
### 4. Paraphrase the Sleep is your Superpower video in your own words in brief?
* Sleep can reduce anxiety.
* Adequate sleep contributes to reducing heart problems.
* Sleep increases our life span.
* Sleep boosts creativity.
### 5. What are some ideas that you can implement to sleep better?
* By avoiding the screens before bed.
* By making the bedroom comfortable for sleep.
* By Maintaining a strict sleep schedule.
### 6. Paraphrase the video - Brain Changing Benefits of Exercise?
* Exercise makes our brain better.
* Exercise gives good stuff to our brain.
* Exercise helps our brain grow.
* Exercise makes you smarter.
* Exercise keeps our brain strong.
### 7. What are some steps you can take to exercise more?
* Reward ourself for reaching milestones.
* Schedule exercise into our daily routine.
* Start small with short walks or gentle exercises.
* Set specific golas for ourself, like walking for 30 minutes a day. 