# SEXUAL HARASSMENT

## What kinds of behaviour cause sexual harassment?
Some forms of sexual harassment include:
+ Making conditions of employment or advancement dependent on sexual favors, either explicitly or implicitly.

+ Physical acts of sexual assault.

+ Requests for sexual favors.

+ Verbal harassment of a sexual nature, including jokes referring to sexual acts or sexual orientation.

+ Unwanted touching or physical contact.

+ Unwelcome sexual advances.

+ Discussing sexual relations/stories/fantasies at work, school, or in other inappropriate places.

+ Feeling pressured to engage with someone sexually.

+ Exposing oneself or performing sexual acts on oneself.

+ Unwanted sexually explicit photos, emails, or text messages.

## What would you do in case you face or witness any incident or repeated incidents of such behaviour?
+ Get informed.
+ Keep a record. Write a detailed description of the incident(s) including what happened, where it occurred, when it took place and if there were any witnesses.
+ Ask them to stop.
+ Address it.
+ Contact emergency services.
