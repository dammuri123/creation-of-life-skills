# CACHING

## What is CACHING?
+ Caching is a technique of storing frequently used information in memory.
+ Cache memory is very near to the CPU.

![Cache Memory](https://cdn.ttgtmedia.com/rms/onlineImages/storage_cache_memory.png)
## When it is used?
+ When the same information is needed next time, it could be directly retrieved from the cache memory.
+ It is important for data-related transactions. 

## Approaches of Caching
    There are two common approaches, They are:
    1. Cache-Aside(Lazy Loading)
    2. Write-Through

### Cache-Aside(lazy Loading):
+ It is a reactive approach and it is updated after the data is requested.

+ The data retrieval logic can be summarized as follows:
1. When your application needs to read data from the database, it checks the cache first to determine whether the data is available or not.
2. If the data is available, the cached data is returned, and the response is issued to the caller.
3. If the data is not available, the database is queried for the data. The cache is then populated with the data that is retrieved from the database, and the data is returned to the caller.

![](https://docs.aws.amazon.com/images/whitepapers/latest/database-caching-strategies-using-redis/images/image3.png)

### Advantages:
1. Simple.
2. Straightforward implementation.

### Disadvantages:
 Requires additional code to manage the cache.

### Write-Through:
+ It is a proactive approach and it is updated immediately when the primary database is updated.

+ In this Write-Through caching, Instead of lazy loading the data in the cache after a cache miss, the cache is proactively updated immediately following the primary database update. The data retrieval logic can be summarized as follows:
1. The application, backend process updates the primary database.
2. Immediately afterward, the data is also updated in the cache.

![](https://docs.aws.amazon.com/images/whitepapers/latest/database-caching-strategies-using-redis/images/image4.png) 

### Advantages:
+ Avoids data loss and corruption in case of power failure or system crash.

### Disadvantages:
+ Larger and more expensive cache.


## Reference

[Cache Memory Diagram](https://cdn.ttgtmedia.com/rms/onlineImages/storage_cache_memory.png)

[Cache-aside diagram](https://docs.aws.amazon.com/images/whitepapers/latest/database-caching-strategies-using-redis/images/image3.png)

[Write-Through diagram](https://docs.aws.amazon.com/images/whitepapers/latest/database-caching-strategies-using-redis/images/image4.png)

[Caching approaches](https://docs.aws.amazon.com/whitepapers/latest/database-caching-strategies-using-redis/caching-patterns.html)






