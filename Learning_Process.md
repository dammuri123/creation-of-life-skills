## What is the Feynman Technique? #
The Feynman Technique is a learning strategy that involves teaching a concept in simple terms to enhance understanding and identify gaps in one's knowledge
## In this video, what was the most interesting story or idea for you?
+ In this video, I liked the focus and discussion mode. when we are in the continuous focus mode our brain does not give broad thoughts because due to experience our brain doesn't give broad ideas.
+ To get rid of this we will go through relax mode and then when we get in focus mode again our brain works fine and gives good logical thoughts. Scientists also follow this technic.
## What are active and diffused modes of thinking?
+ When we are in active mode our brain doesn't focus because due to past experiences, our brain doesn't focus well and is unable to think broadly. To get out of active mode we will go to diffused mode.
+ In diffused mode our brain will relax and again come into active mode then it starts thinking broadly.
## 4. According to the video, what are the steps to take when approaching a new topic? Only mention the points?
+ Learn yourself to self-correct.
+ Practice at least 20 hours.
+ Deconstruct the skills.
+ Remove practice barriers.
 
## 5. What are some of the actions you can take going forward to improve your learning process?
+ Use the Pomodoro technique.
+ Use Feynman technique.
+ Use diffused mode of technique to be creative.